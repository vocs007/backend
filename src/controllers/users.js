const mongoose = require('mongoose')
const Users = require('../models/users')
const Adverts = require('../models/advert')
const config = require('../config/keys')
const fs = require('fs')

const getUser = async function(req) {
    const ObjectID = mongoose.mongo.ObjectId;
    if (req.params.id) {
        const id = req.params.id
        const user = await Users.findOne({ _id: ObjectID(id) })
        const res = {
            name: user.name,
            _id: user._id,
            email: user.email,
            image: user.image,
            role: user.role,
            isBlocked: user.isBlocked
        }
        return res
    }
}

const getUsers = async function() {
    return await Users.find()
}
const updateUser = async function(req, h) {
    console.log(req.payload._id)
    const ObjectID = mongoose.mongo.ObjectId;
    const id = req.payload._id
    const file = req.payload.image
    let name = Date.now() + file.hapi.filename;
    let path = config.filesdir + name;
    if (file) {
        let image = fs.createWriteStream(path);
        image.on('error', function(err) {
            console.error(err)
        });
        file.pipe(image);
        file.on('end', async function(err) {
            await Users.findByIdAndUpdate({ _id: ObjectID(id) }, { image: config.imgUrl + config.filesdir + name })
                .then((res) => {
                    console.log(res)
                    return res.image
                })
                .catch((error) => console.log(error))
        })
        let imgUrl = config.imgUrl + config.filesdir + name
        return imgUrl
    }

}
const banUser = async function(req, h) {
    console.log(req.payload)
    const ObjectID = mongoose.mongo.ObjectId;
    const id = req.payload.admin;
    const userId = req.payload.id
    const admin = await Users.findOne({ _id: ObjectID(id) })
    if (admin.role === 'Admin') {
        const user = await Users.findOne({ _id: ObjectID(userId) })
        console.log('USER: ', user)
        if (user.role != 'Admin') {
            Users.findByIdAndUpdate({ _id: ObjectID(userId) }, { isBlocked: !user.isBlocked })
                .then(() => {
                    if (user.isBlocked === false) {
                        Adverts.deleteMany({ 'creator.id': ObjectID(userId) })
                            .then((res) => {
                                console.log('Deleted adverts: ', res)
                            })
                    }
                })
        }
    } else return h.response().code(401)
    return true
}

module.exports = {
    getUser,
    updateUser,
    banUser,
    getUsers
}