const mongoose = require('mongoose')
const Advert = require('../models/advert')
const config = require('../config/keys')
const fs = require('fs')

const getAdverts = async function(req) {
    console.log(req.params)
    const options = {
        page: req.params.page,
        limit: 6,
        collation: {
            locale: 'en',
        },
        sort: {
            date: -1
        }
    }
    if (req.params.category != '' && req.params.category != 'undefined') {
        return await Advert.paginate({ "category": req.params.category }, options)
    } else {
        return await Advert.paginate({}, options)
    }
};

const getImage = async function(req, h) {
    let path = req.query.url;
    return h.file(path)
}

const getUserAdverts = async function(req) {
    const ObjectID = mongoose.mongo.ObjectId;
    const id = req.params.id;
    return await Advert.find({ "creator.id": ObjectID(id) });
}

const getAdvert = async function(req) {
    const ObjectID = mongoose.mongo.ObjectId;
    if (req.params.id) {
        const id = req.params.id
        return await Advert.findOne({ _id: ObjectID(id) })
    }
}

const deleteAdvert = async function(req) {
    const ObjectID = mongoose.mongo.ObjectId;
    if (req.payload.advert && req.payload.user) {
        const advert = req.payload.advert
        const user = req.payload.user
        if (advert.creator.id === user.id || user.role === 'Admin') {
            console.log(advert._id)
            return await Advert.deleteOne({ _id: ObjectID(advert._id) })
                .then((res) => {
                    return res
                })
                .catch((error) => { return error })
        }
    }
}
const createAdvert = async function(req) {
    let data = req.payload;
    console.log(data)
    let name = Date.now() + data.file.hapi.filename;
    let path = config.filesdir + name;
    if (data.file) {
        let file = fs.createWriteStream(path);
        file.on('error', function(err) {
            console.error(err)
        });
        data.file.pipe(file);
        data.file.on('end', function(err) {
            let ret = {
                filename: data.file.hapi.filename,
                headers: data.file.hapi.headers
            }
            return ret;
        })
        console.log(file)
    }
    const advert = new Advert({
        title: data.title,
        text: data.text,
        creator: {
            id: data.id,
            name: data.name
        },
        file: config.imgUrl + path,
        category: data.category
    })
    return await Advert.create(advert)
        .then((res) => {
            return res
        })
        .catch((error) => {
            return error.response.status.code(401);
        });
}

const updateAdvert = async function(req) {
    const ObjectID = mongoose.mongo.ObjectId;
    let data = req.payload
    console.log(data)
    if (data.file != 'undefined') {
        let name = Date.now() + data.file.hapi.filename;
        let path = config.filesdir + name;
        let file = fs.createWriteStream(path);
        file.on('error', function(err) {
            console.error(err)
        });
        data.file.pipe(file);
        data.file.on('end', function(err) {
            let ret = {
                filename: data.file.hapi.filename,
                headers: data.file.hapi.headers
            }
            return Advert.findByIdAndUpdate({ _id: ObjectID(req.params.id) }, { file: config.imgUrl + config.filesdir + name, title: data.title, text: data.text })
        })
    } else {
        return Advert.findByIdAndUpdate({ _id: ObjectID(req.params.id) }, { title: data.title, text: data.text })
            .then((res) => {
                console.log(res)
                return res
            })
            .catch((error) => {
                console.log(error)
                return error
            })

    }

}

module.exports = {
    getAdverts,
    getAdvert,
    getUserAdverts,
    createAdvert,
    deleteAdvert,
    getImage,
    updateAdvert
}