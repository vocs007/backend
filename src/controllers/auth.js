const jsonwebtoken = require("jsonwebtoken")
const bcrypt = require("bcryptjs")
const User = require('../models/users')
const keys = require('../config/keys')


const login = async function(req, h) {
    const user = await User.findOne({ email: req.payload.email })
    if (user) {
        const validPass = await bcrypt.compare(req.payload.password, user.password)
        if (validPass) {
            if (user.isBlocked) {
                return h.response().code(402)
            }
            const token = jsonwebtoken.sign({ email: user.email, id: user._id }, keys.jwt, { expiresIn: 60 * 60 })
            console.log('Generated', token)
            const userInfo = {
                id: user.id,
                name: user.name,
                role: user.role,
                image: user.image,
            }
            return JSON.stringify({ token: `Bearer ${token}`, userInfo });
        } else {
            return h.response().code(401)
        }
    } else {
        return h.response().code(404)
    }
}

const registration = async function(req, h) {
    return await User.create({
        name: req.payload.name,
        email: req.payload.email,
        password: req.payload.password,
        isBlocked: false,
        role: 'User'
    })
}

module.exports = {
    login,
    registration
}