'use strict';
const initRoutes = require('./routes/routes.js');
const Hapi = require('@hapi/hapi');
const User = require('./models/users')

const init = async() => {
    const server = Hapi.server({
        port: 3000,
        host: 'localhost',
        routes: {
            cors: {
                origin: ['*']
            }
        }
    });
    const validate = async function(decoded) {
        const user = await User.findOne({ email: decoded.email })
        if (user.email === decoded.email) {
            return { isValid: true }
        } else {
            return { isValid: false }
        }
    };
    await server.register(require('@hapi/inert'));
    await server.register(require('hapi-auth-jwt2'));
    server.auth.strategy('jwt', 'jwt', {
        key: 'secret',
        validate
    });
    server.auth.default('jwt');

    initRoutes(server);
    await server.start();
    console.log('Server running on %s', server.info.uri);

};

process.on('unhandledRejection', (err) => {
    console.log(err);
    process.exit(1);
});

init();