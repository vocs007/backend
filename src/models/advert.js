const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
const Schema = mongoose.Schema;

const AdvertSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    text: {
        type: String,
        required: true
    },
    creator: {
        id: String,
        name: String
    },
    file: String,
    date: {
        type: Date,
        default: Date.now,
    },
    category: String
});

AdvertSchema.plugin(mongoosePaginate);
AdvertSchema.index({ title: 'text', text: 'text' });
module.exports = mongoose.model('Advert', AdvertSchema, "advert");