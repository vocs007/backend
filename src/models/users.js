const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    name: {
        type: String,
        unique: false
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true,
        unique: false
    },
    isBlocked: Boolean,
    role: {
        type: String,
        unique: false
    },
    image: {
        type: String,
        default: 'http://localhost:3000/image/?url=C:/SoftServe/Server/assets/profiles/profile.jpg'
    }
});
UserSchema.plugin(require('mongoose-bcrypt'));
module.exports = mongoose.model('User', UserSchema);