const mongoose = require('mongoose');
const db = require('../dbconfig');
const auth = require('../controllers/auth');
const adverts = require('../controllers/adverts')
const categories = require('../controllers/categories')
const users = require('../controllers/users')

mongoose.set('useCreateIndex', true);
mongoose.connect('mongodb://127.0.0.1:' + db.port + '/' + db.name, { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false });
mongoose.connection.once('open', () => {
    console.log('Connected to database TEST');
});

module.exports = function initRoutes(server) {
    server.route({
        method: 'GET',
        path: '/adverts/{page}/{category?}',
        options: {
            auth: false
        },
        handler: adverts.getAdverts
    });

    server.route({
        method: 'GET',
        path: '/image/',
        options: {
            auth: false
        },
        handler: adverts.getImage
    });

    server.route({
        method: 'GET',
        path: '/adverts/user/{id}',
        options: {
            auth: false
        },
        handler: adverts.getUserAdverts
    });

    server.route({
        method: 'GET',
        path: '/advert/{id}',
        options: {
            auth: false
        },
        handler: adverts.getAdvert
    });

    server.route({
        method: 'GET',
        path: '/user/{id}',
        config: { auth: 'jwt' },
        handler: users.getUser
    });

    server.route({
        method: 'GET',
        path: '/users',
        config: { auth: 'jwt' },
        handler: users.getUsers
    });

    server.route({
        method: 'GET',
        path: '/categories',
        config: { auth: false },
        handler: categories.getCategories
    });

    server.route({
        method: 'POST',
        path: '/login',
        options: {
            auth: false,
            payload: {
                parse: true,
                multipart: true
            }
        },
        handler: auth.login
    });

    server.route({
        method: 'POST',
        path: '/register',
        options: {
            auth: false,
            payload: {
                parse: true,
                multipart: true
            }
        },
        handler: auth.registration
    });

    server.route({
        method: 'DELETE',
        path: '/deleteadvert',
        options: {
            auth: 'jwt',
            payload: {
                parse: true,
                multipart: true
            }
        },
        handler: adverts.deleteAdvert
    });

    server.route({
        method: 'POST',
        path: '/createadvert',
        options: {
            auth: 'jwt',
            payload: {
                maxBytes: 5242880,
                output: 'stream',
                parse: true,
                multipart: true
            }
        },
        handler: adverts.createAdvert
    });

    server.route({
        method: 'PUT',
        path: '/profile/{req}',
        options: {
            auth: 'jwt',
            payload: {
                maxBytes: 5242880,
                output: 'stream',
                parse: true,
                multipart: true
            }
        },
        handler: users.updateUser
    })

    server.route({
        method: 'PUT',
        path: '/modify/{id}',
        options: {
            auth: 'jwt',
            payload: {
                maxBytes: 5242880,
                output: 'stream',
                parse: true,
                multipart: true
            }
        },
        handler: adverts.updateAdvert
    })

    server.route({
        method: 'POST',
        path: '/ban/{id}',
        options: {
            auth: false,
            payload: {
                parse: true,
                multipart: true
            }
        },
        handler: users.banUser
    });

    //     server.route({
    //         method: 'GET',
    //         path: '/search/{search}',
    //         options: {
    //             auth: false,
    //         },
    //         handler: adverts.findAdverts
    //     });
};